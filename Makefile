include .env

init:
	cp .env.dist .env
	docker-compose pull
	make app-init

stop:
	docker-compose stop

down:
	docker-compose down

start:
	make app-start

app-init:
	docker-compose run --rm node npm install

app-start:
	docker-compose up -d

logs:
	docker logs -f chess-train-back

test-unit:
	docker-compose run --rm node npm run test:unit

test-coverage:
	docker-compose run --rm node npm run test:coverage

test-unit-watch:
	docker-compose run --rm node npm run test:watch

test-integration:
	docker-compose run --rm node npm run test:e2e

build:
	docker-compose run --rm node npm run build

database-start:
	docker-compose up -d

database-reset:
	docker-compose down -v
	make database-start
	sleep 5
	docker-compose run --rm node npm run knex:local migrate:latest -x ts
	sleep 5
	docker-compose run --rm node npm run knex:local seed:run -x ts

database-migrate:
	docker-compose run --rm node npm run knex:local migrate:latest -x ts

prettier:
	docker-compose run node npm run format