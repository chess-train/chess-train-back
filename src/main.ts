import { AppModule } from '@/app.module'
import config from '@/config/config'
import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { ChessLogger } from './logger/chess.logger'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true })
  app.useLogger(new ChessLogger())
  app.enableCors()
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }))

  // Start app
  const port = config.get('PORT')
  await app.listen(port)
}

bootstrap()
