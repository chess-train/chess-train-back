import { Knex } from 'knex'
import { KNEX_CLIENT } from './knex.constants'
import { Inject } from '@nestjs/common'

export class KnexRepository<
  TRecord extends { id: string } = any,
  TOmitInsertAttributes extends string | number | symbol = 'id' | 'createdAt' | 'updatedAt',
> {
  public tableName = 'default'
  public get queryBuilder(): Knex.QueryBuilder<TRecord> {
    return this.knex(this.tableName)
  }
  constructor(@Inject(KNEX_CLIENT) protected readonly knex: Knex<TRecord>) {}

  async insert(record: Omit<TRecord, TOmitInsertAttributes>): Promise<TRecord> {
    const newRecord = await this.queryBuilder.insert(record as any).returning('*')

    return newRecord[0] as TRecord
  }

  async insertMany(records: Array<Omit<TRecord, TOmitInsertAttributes>>): Promise<TRecord[]> {
    const newRecords = await this.queryBuilder.insert(records as any).returning('*')

    return newRecords as TRecord[]
  }

  find(where?: Partial<TRecord>, limit?: number, offset?: number): Promise<TRecord[]> {
    return this.queryBuilder
      .where(where ?? {})
      .modify((queryBuilder: Knex.QueryBuilder) => {
        if (typeof offset === 'number') {
          queryBuilder.offset(offset)
        }
        if (typeof limit === 'number') {
          queryBuilder.limit(limit)
        }
      })
      .orderBy('id', 'ASC')
  }

  async findById(id: string): Promise<TRecord | undefined> {
    const record = await this.queryBuilder.where('id', id).first()
    if (!record) {
      return
    }
    return record as TRecord
  }

  async update(where: Partial<TRecord>, record: Partial<TRecord>): Promise<TRecord[] | undefined> {
    const updatedRecords = await this.queryBuilder
      .where(where)
      .update(record as any)
      .returning('*')
    if (updatedRecords.length === 0) {
      return
    }
    return updatedRecords as TRecord[]
  }

  async updateById(id: string, record: Partial<TRecord>): Promise<TRecord | undefined> {
    const updatedRecords = await this.queryBuilder
      .where('id', id)
      .update(record as any)
      .returning('*')
    if (updatedRecords.length === 0) {
      return
    }
    return updatedRecords[0] as TRecord
  }

  delete(where: Partial<TRecord>): Promise<number> {
    return this.queryBuilder.where(where).del()
  }

  deleteById(id: string): Promise<number> {
    return this.queryBuilder.where('id', id).del()
  }
}
