import { Module, Global, DynamicModule } from '@nestjs/common'
import { KnexClient, KnexOptions } from './knex.client'
import { KNEX_CLIENT } from './knex.constants'

@Global()
@Module({})
export class KnexModule {
  static register(options: KnexOptions): DynamicModule {
    return {
      module: KnexModule,
      providers: [
        {
          provide: KNEX_CLIENT,
          useFactory: () => {
            return KnexClient(options)
          },
        },
      ],
      exports: [KNEX_CLIENT],
    }
  }
}
