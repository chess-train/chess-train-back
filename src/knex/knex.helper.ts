export const updateTriggerCode = `
    CREATE OR REPLACE FUNCTION update_updated_at()
        RETURNS TRIGGER AS $$
        BEGIN
        NEW."updatedAt" = now();
        RETURN NEW;
    END;
    $$ language 'plpgsql';
`

export const triggerUpdate = (tableName: string): string => {
  return `
        CREATE TRIGGER update_${tableName}_updated_at
        BEFORE UPDATE ON "${tableName}"
        FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at();
    `
}
