import { knex, Knex } from 'knex'

export type KnexOptions = Omit<Knex.Config, 'debug'> & {
  debug: boolean | string[]
}

export const KnexClient = (options: KnexOptions): Knex => knex(options as Knex.Config)
