import { databaseConfig } from './database.common'

module.exports = {
  ...databaseConfig,
  migrations: {
    tableName: 'knex_migrations',
    directory: '../../database/migrations',
  },
  seeds: {
    directory: '../../database/seeds',
  },
}
