import config from './config'

export const databaseConfig = {
  client: config.get('DATABASE_DIALECT'),
  version: config.get('DATABASE_VERSION'),
  connection: {
    user: config.get('DATABASE_USERNAME'),
    password: config.get('DATABASE_PASSWORD'),
    database: config.get('DATABASE_NAME'),
    host: config.get('DATABASE_HOST'),
    port: config.get('DATABASE_PORT'),
  },
  debug: config.get('LOG_LEVEL') === 'debug' ? ['ComQueryPacket'] : false,
  pool: {
    min: config.get('DATABASE_POOL_MINIMUM'),
    max: config.get('DATABASE_POOL_MAXIMUM'),
  },
}
