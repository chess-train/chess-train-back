import convict from 'convict'
import * as dotenv from 'dotenv'
import { version } from '../../package.json'

dotenv.config({ path: '../../.env' })

const config = convict({
  APP_NAME: {
    doc: 'Name of this service',
    format: String,
    default: 'chess-train-back',
    env: 'APP_NAME',
  },
  APP_VERSION: {
    doc: 'Version of the service',
    format: String,
    default: '',
  },
  NODE_ENV: {
    doc: 'The application environment.',
    format: ['development', 'test', 'production', null],
    default: 'production',
    env: 'NODE_ENV',
  },
  HOST: {
    doc: 'Hostname of this application',
    format: String,
    default: '0.0.0.0',
    env: 'HOST',
  },
  PORT: {
    doc: 'Exposed port of this application',
    format: Number,
    default: 3000,
    env: 'PORT',
  },
  LOG_LEVEL: {
    doc: 'Activate logs in console',
    format: ['critical', 'error', 'warning', 'info', 'debug'],
    default: 'info',
    env: 'LOG_LEVEL',
  },
  DATABASE_USERNAME: {
    doc: 'Username for the database connection',
    format: String,
    default: 'postgres',
    env: 'DATABASE_USERNAME',
  },
  DATABASE_PASSWORD: {
    doc: 'Password for the database connection',
    format: String,
    default: 'password',
    env: 'DATABASE_PASSWORD',
  },
  DATABASE_NAME: {
    doc: 'Database name of this application',
    format: String,
    default: 'database',
    env: 'DATABASE_NAME',
  },
  DATABASE_HOST: {
    doc: 'Database hostname of this application',
    format: String,
    default: '127.0.0.1',
    env: 'DATABASE_HOST',
  },
  DATABASE_PORT: {
    doc: 'Database port of this application',
    format: Number,
    default: 5432,
    env: 'DATABASE_PORT',
  },
  DATABASE_DIALECT: {
    doc: 'Database dialect of this application',
    format: String,
    default: 'pg',
    env: 'DATABASE_DIALECT',
  },
  DATABASE_VERSION: {
    doc: 'Database version of this application',
    format: String,
    default: '13.2',
    env: 'DATABASE_VERSION',
  },
  DATABASE_POOL_MINIMUM: {
    doc: 'Database pool minimum connection',
    format: Number,
    default: 0,
    env: 'DATABASE_POOL_MINIMUM',
  },
  DATABASE_POOL_MAXIMUM: {
    doc: 'Database pool maximum connection',
    format: Number,
    default: 5,
    env: 'DATABASE_POOL_MAXIMUM',
  },
  GOOGLE_CLIENT_ID: {
    doc: 'Google app client id for oauth',
    format: String,
    default: '',
    env: 'GOOGLE_CLIENT_ID',
  },
  GOOGLE_SECRET: {
    doc: 'Google app secret for oauth',
    format: String,
    default: '',
    env: 'GOOGLE_SECRET',
  },
  GOOGLE_REDIRECT_URL: {
    doc: 'URL used by google for auth redirect',
    format: String,
    default: 'http://localhost:3010/auth/google/redirect',
    env: 'GOOGLE_REDIRECT_URL',
  },
  JWT_SECRET: {
    doc: 'Secret to sign JWT',
    format: String,
    default: '',
    env: 'JWT_SECRET',
  },
  FRONTEND_URL: {
    doc: 'Frontend global url',
    format: String,
    default: null,
    env: 'FRONTEND_URL',
  },
  FRONTEND_LOGIN_SUCCESS: {
    doc: 'Frontend login success callback url',
    format: String,
    default: '',
    env: 'FRONTEND_LOGIN_SUCCESS',
  },
})

config.set('APP_VERSION', version)

export default config
