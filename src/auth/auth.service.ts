import { UserRecord } from '@/users/users.interface'
import { UsersService } from '@/users/users.service'
import { Injectable, NotFoundException } from '@nestjs/common'
import { v4 } from 'uuid'
import config from '@/config/config'
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class AuthService {
  private APP_NAME = config.get('APP_NAME')

  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async googleLogin(req: Record<string, any>): Promise<string> {
    if (!req.user) {
      throw new NotFoundException('No user from Google')
    }

    const { email, authProvider, authId, givenName, familyName } = req.user

    const users = await this.usersService.find({
      email,
      authProvider,
      authId,
    })

    if (users.length === 1) {
      return this.generateJWT(users[0])
    }

    const user = await this.usersService.create({
      id: v4(),
      email,
      authId,
      authProvider,
      givenName,
      familyName,
    })

    return this.generateJWT(user)
  }

  async validateUserFromJwt(authId: string, authProvider: string): Promise<UserRecord> {
    const users = await this.usersService.find({
      authProvider,
      authId,
    })

    if (users.length === 1) {
      return users[0]
    }
    throw new NotFoundException()
  }

  generateJWT({ authProvider, authId }: UserRecord): string {
    return this.jwtService.sign({ authId, authProvider, iss: this.APP_NAME })
  }
}
