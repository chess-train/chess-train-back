import { Strategy, VerifyCallback } from 'passport-google-oauth2'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable } from '@nestjs/common'
import { AuthService } from './auth.service'
import config from '@/config/config'

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private authService: AuthService) {
    super({
      clientID: config.get('GOOGLE_CLIENT_ID'),
      clientSecret: config.get('GOOGLE_SECRET'),
      callbackURL: config.get('GOOGLE_REDIRECT_URL'),
      scope: ['email', 'profile'],
    })
  }

  async validate(
    accessToken: string,
    _refreshToken: string,
    profile: Record<string, any>,
    _done: VerifyCallback,
  ): Promise<any> {
    const {
      name: { givenName, familyName },
      email,
      provider,
      id,
    } = profile

    return {
      email,
      givenName,
      familyName,
      accessToken,
      authProvider: provider,
      authId: id,
    }
  }
}
