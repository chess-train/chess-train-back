import { Controller, Get, Req, Res, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { AuthService } from './auth.service'
import { GetCurrentUser } from '@/auth/get-current-user.decorator'
import { JwtAuthGuard } from '@/auth/jwt-auth.guard'
import { UserRecord } from '@/users/users.interface'
import { Response } from 'express'
import config from '@/config/config'

const FRONTEND_URL = config.get('FRONTEND_URL')
const frontendSuccessCallback = `${FRONTEND_URL}${config.get('FRONTEND_LOGIN_SUCCESS')}`

@Controller('auth')
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @Get('google')
  @UseGuards(AuthGuard('google'))
  async googleAuth(@Req() _req: Record<string, any>) {
    // blank. Used to redirect to google to authenticate user against oauth
  }

  @Get('google/redirect')
  @UseGuards(AuthGuard('google'))
  async googleAuthRedirect(
    @Req() req: Record<string, any>,
    @Res() res: Response,
  ): Promise<{ token: string } | void> {
    const token = await this.service.googleLogin(req)

    if (FRONTEND_URL) {
      return res.redirect(frontendSuccessCallback.replace('{{token}}', token))
    }

    res.status(201).send({ token })
  }

  @Get('me')
  @UseGuards(JwtAuthGuard)
  async whoAmI(
    @GetCurrentUser() user: UserRecord,
  ): Promise<{ user: Pick<UserRecord, 'id' | 'email' | 'familyName' | 'givenName'> }> {
    return {
      user: {
        email: user.email,
        id: user.id,
        familyName: user.familyName,
        givenName: user.givenName,
      },
    }
  }
}
