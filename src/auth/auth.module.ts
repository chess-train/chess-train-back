import { UsersModule } from '@/users/users.module'
import { Module } from '@nestjs/common'
import { JwtModule } from '@nestjs/jwt'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { GoogleStrategy } from './google.strategy'
import config from '@/config/config'
import { JwtStrategy } from './jwt.strategy'

@Module({
  providers: [AuthService, GoogleStrategy, JwtStrategy],
  controllers: [AuthController],
  imports: [
    UsersModule,
    JwtModule.register({
      secret: config.get('JWT_SECRET'),
      signOptions: { expiresIn: '1h' },
    }),
  ],
})
export class AuthModule {}
