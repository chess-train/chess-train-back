import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable } from '@nestjs/common'
import config from '@/config/config'
import { AuthService } from './auth.service'
import { UserRecord } from '@/users/users.interface'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get('JWT_SECRET'),
    })
  }

  async validate({ authId, authProvider }: Record<string, any>): Promise<UserRecord> {
    return this.authService.validateUserFromJwt(authId, authProvider)
  }
}
