import { AppController } from '@/app.controller'
import { databaseConfig } from '@/config/database.common'
import { KnexModule } from '@/knex/knex.module'
import { Module } from '@nestjs/common'
import { AuthModule } from './auth/auth.module'
import { ChessjsModule } from './chessjs/chessjs.module'
import { GameModule } from './game/game.module'
import { UsersModule } from './users/users.module'
import { LoggerModule } from './logger/logger.module'

@Module({
  imports: [
    KnexModule.register(databaseConfig),
    GameModule,
    ChessjsModule,
    UsersModule,
    AuthModule,
    LoggerModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
