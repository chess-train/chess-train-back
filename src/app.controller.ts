import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common'

@Controller()
export class AppController {
  @Get('/health')
  @HttpCode(HttpStatus.NO_CONTENT)
  health(): void {
    return
  }
}
