import { Module } from '@nestjs/common'
import { ChessLogger } from './chess.logger'

@Module({
  providers: [ChessLogger],
  exports: [ChessLogger],
})
export class LoggerModule {}
