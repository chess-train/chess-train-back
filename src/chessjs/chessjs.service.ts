import { BadRequestException, Injectable } from '@nestjs/common'
import { Chess, ChessInstance } from 'chess.js'

@Injectable()
export class ChessjsService {
  initBoard(fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'): ChessInstance {
    return new Chess(fen)
  }

  initBoardFromPgn(pgn: string): ChessInstance {
    const chess = new Chess()
    const loaded = chess.load_pgn(pgn, { sloppy: true })

    if (loaded) {
      return chess
    }

    throw new BadRequestException('Incorrect PGN provided')
  }

  move(move: string, chess: ChessInstance): ChessInstance {
    const movement = chess.move(move, { sloppy: true })

    if (movement) {
      return chess
    }

    throw new BadRequestException('Forbidden or invalid move')
  }

  canColorPlay(currentUserColor: 'b' | 'w', chess: ChessInstance): boolean {
    return this.getTurn(chess) === currentUserColor
  }

  getValidMoves(chess: ChessInstance): string[] {
    return chess.moves()
  }

  getTurn(chess: ChessInstance): 'b' | 'w' {
    return chess.turn()
  }

  getState(chess: ChessInstance) {
    if (chess.game_over()) {
      if (chess.in_draw()) {
        return { state: 'DRAW' }
      }
      const history = chess.history({ verbose: true })
      return { state: 'GAME_OVER', winner: history[history.length - 1].color }
    }

    const { Result = '*' } = chess.header()
    switch (Result.trim()) {
      case '1-0':
        return { state: 'GAME_OVER', winner: 'w' }
      case '0-1':
        return { state: 'GAME_OVER', winner: 'b' }
      case '1/2-1/2':
        return { state: 'DRAW' }
      case '*':
      default:
        return { state: 'RUNNING' }
    }
  }

  setComment(chess: ChessInstance, comment: string): ChessInstance {
    chess.set_comment(comment)
    return chess
  }
}
