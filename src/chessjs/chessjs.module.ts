import { Module } from '@nestjs/common'
import { ChessjsService } from './chessjs.service'

@Module({
  providers: [ChessjsService],
  exports: [ChessjsService],
})
export class ChessjsModule {}
