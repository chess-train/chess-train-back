import { Test, TestingModule } from '@nestjs/testing'
import { ChessjsService } from './chessjs.service'

const defaultStartingFen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
const emptyBoardFen = '8/8/8/8/8/8/8/8 w - - 0 1'
const randomFen = 'r2qkbnr/ppp2ppp/2n5/1B2pQ2/4P3/8/PPP2PPP/RNB1K2R b KQkq - 3 7'
const incorrectFen = 'r2qkbnr/ppp2ppp/2n5/1B2pQ2/4P3/8/PPP2PPP/RNB1KR b KQkq - 3 7'
const e4Fen = 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1'

const validPGN = `[Event "Interclubs FRA"]
[Site "?"]
[Date "1999.01.01"]
[Round "?"]
[White "Calistri, Tristan"]
[Black "Bauduin, Etienne"]
[Result "1-0"]

1.e4 c5 2.Nf3 e6 3.d4 cxd4 4.Nxd4 Nc6 5.Nc3 a6 6.Be2 Qc7 7.O-O Nf6 8.Be3 Bb4 
9.Na4 O-O 10.c4 Bd6 11.g3 Nxe4 12.Bf3 f5 13.Bxe4 fxe4 14.c5 Be7 {Les Noirs ont 
un pion d'avance mais de gros problèmes pour mettre leur Fc8 et leur Ta8 en jeu} 
15.Qg4 Ne5 16.Qxe4 d5 17.cxd6 Bxd6 18.Rac1 Qa5 19.Nb3 {Les blancs ont 
récupéré leur pion et toutes leurs pièces sont mobilisées}
19...Qb4 
20.Qxb4 Bxb4 21.Nb6 $18 {Les noirs n'arriveront jamais à sortir leur Fc8}
21...Rb8 22.Bc5 Bxc5 
23.Nxc5 Rd8 24.Rfd1 Re8 25.Ne4 Nf7 26.Rc7 Kf8 27.Rdc1 1-0`
const finalFen = '1rb1rk2/1pR2npp/pN2p3/8/4N3/6P1/PP3P1P/2R3K1 b - - 8 27'

const invalidPGN = `
1.e4 c5 2.Nf3 e6 3.d4 cxd4 4.Nxd4 Nc6 5.Nc3 a6 6.Be2 Qc7 7.O-O Nf6 8.Be3 Bb4 
9.Na4 O-O 10.c4 Bd6 11.g3 Nxe4 12.Bf3 f5 13.Bxe4 fxe4 14.c5 Be7 {Les Noirs ont 
un pion d'avance mais de gros problèmes pour mettre leur Fc8 et leur Ta8 en jeu} 
15.Qg4 Ne5 16.Qxe4 d5 17.cxd6 Bxd6 18.Rac1 Qa5 19.Nb3 {Les blancs ont 
récupéré leur pion et toutes leurs pièces sont mobilisées}
19...Qb4 
21...Rb8 22.Bc5 Bxc5 
23.Nxc5 Rd8 24.Rfd1 Re8 25.Ne4 Nf7 26.Rc7 Kf8 27.Rdc1 1-0`

const validVictoryPGN = `[Event "Casual Correspondence game"]
[Site "https://lichess.org/"]
[Date "2021.05.09"]
[White "lichess AI level 1"]
[Black "BlackPlayer"]
[Result "0-1"]
[UTCDate "2021.05.09"]
[UTCTime "15:58:54"]
[WhiteElo "?"]
[BlackElo "1500"]
[Variant "Standard"]
[TimeControl "-"]
[ECO "A00"]
[Opening "Mieses Opening"]
[Termination "Normal"]
[Annotator "lichess.org"]

1. d3 { A00 Mieses Opening } d5 2. c3 Nf6 3. Nf3 Bf5 4. Qb3 Nc6 5. Nd4 e6 6. h3 Bc5 7. Nxf5 exf5
8. Bf4 O-O 9. e3 g6 10. Nd2 Na5 11. Qa4 b6 12. Be2 Qd7 13. Bd1 Qxa4 14. Bxc7 Qc6 15. Bf4 Rfe8
16. Nf3 Re7 17. Nd4 Rae8 18. O-O Bxd4 19. Bc2 Bxc3 20. Rab1 Nh5 21. Bg5 Re5 22. Rfc1 h6
23. Bh4 g5 24. a3 gxh4 25. Rf1 f4 26. exf4 Nxf4 27. Bd1 Re1 28. Bg4 Nb3 29. Rbxe1 Bxe1
30. Be6 Rxe6 31. a4 Nd2 32. g4 Nxf1 33. f3 Nxh3+ 34. Kg2 d4 35. b4 Nd2 36. a5 Qxf3+
37. Kh2 Re2# { Black wins by checkmate. } 0-1`

const validDrawPGN = `[Event "Casual Correspondence game"]
[Site "https://lichess.org/"]
[Date "2021.05.09"]
[White "lichess AI level 2"]
[Black "BlackPlayer"]
[Result "1/2-1/2"]
[UTCDate "2021.05.09"]
[UTCTime "16:09:51"]
[WhiteElo "?"]
[BlackElo "1500"]
[Variant "Standard"]
[TimeControl "-"]
[ECO "C25"]
[Opening "Vienna Game: Max Lange Defense"]
[Termination "Normal"]
[Annotator "lichess.org"]

1. e4 e5 2. Nc3 Nc6 { C25 Vienna Game: Max Lange Defense } 3. Nd5 Nd4 4. h4 h6 5. b3 b6 6. Bb2 Bb7
7. Nf3 Nf6 8. Ne3 Ne6 9. Bd3 Bd6 10. O-O O-O 11. Bxe5 Bxe5 12. Nxe5 Nxe4 13. Qg4 Qe7 14. Nxf7 Qxf7
15. f3 Qe7 16. fxe4 Qc5 17. Kh1 Qxe3 18. Qxe6+ Kh7 19. Qg4 Qxd3 20. Rf7 Rxf7 21. Qxg7+ Kxg7
22. Rc1 Qxe4 23. Kh2 Qxh4+ 24. Kg1 Qd4+ 25. Kh2 Qf4+ 26. Kg1 Qd4+ 27. Kh2 Qh4+ 28. Kg1 Qd4+
29. Kh1 Qh4+ 30. Kg1 Qd4+ 31. Kh1 Qh4+ 32. Kg1 Qd4+ { The game is a draw. } 1/2-1/2`

const whiteStartingValidMoves = [
  'a3',
  'a4',
  'b3',
  'b4',
  'c3',
  'c4',
  'd3',
  'd4',
  'e3',
  'e4',
  'f3',
  'f4',
  'g3',
  'g4',
  'h3',
  'h4',
  'Na3',
  'Nc3',
  'Nf3',
  'Nh3',
]
const blackStartingValidMoves = [
  'a6',
  'a5',
  'b6',
  'b5',
  'c6',
  'c5',
  'd6',
  'd5',
  'e6',
  'e5',
  'f6',
  'f5',
  'g6',
  'g5',
  'h6',
  'h5',
  'Na6',
  'Nc6',
  'Nf6',
  'Nh6',
]

describe('ChessjsService', () => {
  let service: ChessjsService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChessjsService],
    }).compile()

    service = module.get<ChessjsService>(ChessjsService)
  })

  describe('initBoard()', () => {
    it('Should init a new board with the default starting position', () => {
      const chess = service.initBoard()

      expect(chess.fen()).toBe(defaultStartingFen)
    })

    it('Should init a new board with sent position', () => {
      const chess = service.initBoard(randomFen)

      expect(chess.fen()).toBe(randomFen)
    })

    it('Should init a new board with incorrect FEN and fallback to an empty board', () => {
      const chess = service.initBoard(incorrectFen)

      expect(chess.fen()).toBe(emptyBoardFen)
    })
  })

  describe('initBoardFromPgn()', () => {
    it('Should return a chess instance with the PGN loaded', () => {
      const chess = service.initBoardFromPgn(validPGN)

      expect(chess.header().White).toBe('Calistri, Tristan')
      expect(chess.fen()).toBe(finalFen)
    })

    it('Should throw an error when trying to load an invalid PGN', () => {
      try {
        service.initBoardFromPgn(invalidPGN)
      } catch (err) {
        expect(err.message).toBe('Incorrect PGN provided')
      }
    })
  })

  describe('move()', () => {
    it('Should return a ChessInstance if the move if valid', () => {
      let chess = service.initBoard()
      chess = service.move('e4', chess)

      expect(chess.fen()).toBe(e4Fen)
    })

    it('Should throw and error if move is invalid', () => {
      let chess = service.initBoard()

      try {
        chess.move('Na2')
      } catch (err) {
        expect(err.message).toBe('Forbidden or invalid move')
      }
    })
  })

  describe('getValidMoves()', () => {
    it('Should receive the list of valid moves', () => {
      let chess = service.initBoard()

      expect(
        service.getValidMoves(chess).every((move) => whiteStartingValidMoves.includes(move)),
      ).toBeTruthy()

      chess = service.move('Na3', chess)
      expect(
        service.getValidMoves(chess).every((move) => blackStartingValidMoves.includes(move)),
      ).toBeTruthy()
    })
  })

  describe('getTurn()', () => {
    it('Should tell that the white player have to play then the black one after first move', () => {
      let chess = service.initBoard()
      expect(service.getTurn(chess)).toBe('w')
      chess = service.move('e4', chess)
      expect(service.getTurn(chess)).toBe('b')
    })
  })

  describe('canPlay()', () => {
    it('Should get true if the user is allowed to play', () => {
      let chess = service.initBoard()
      expect(service.canColorPlay('w', chess)).toBeTruthy()
    })

    it('Should get false if the user is not allowed to play (surprisingly)', () => {
      let chess = service.initBoard()
      expect(service.canColorPlay('b', chess)).toBeFalsy()
    })
  })

  describe('getState()', () => {
    it('Should get a running status', () => {
      let chess = service.initBoard(
        'r1bqkb1r/pppp1Bpp/2n2n2/4p3/4P3/5Q2/PPPP1PPP/RNB1K1NR b KQkq - 0 4',
      )

      const state = service.getState(chess)
      expect(state).toStrictEqual({ state: 'RUNNING' })
    })

    it('Should get a GAME_OVER status', () => {
      let chess = service.initBoardFromPgn(validVictoryPGN)
      const state = service.getState(chess)
      expect(state).toStrictEqual({ state: 'GAME_OVER', winner: 'b' })
    })

    it('Should get a GAME_OVER status from PGN header', () => {
      let chess = service.initBoardFromPgn(validPGN)
      const state = service.getState(chess)
      expect(state).toStrictEqual({ state: 'GAME_OVER', winner: 'w' })
    })

    it('Should get a RUNNING status', () => {
      let chess = service.initBoard()

      const state = service.getState(chess)
      expect(state).toStrictEqual({ state: 'RUNNING' })
    })

    it('Should get a DRAW status', () => {
      let chess = service.initBoardFromPgn(validDrawPGN)

      const state = service.getState(chess)
      expect(state).toStrictEqual({ state: 'DRAW' })
    })
  })

  describe('setComment()', () => {
    it('Should save the comment for the current position', () => {
      let chess = service.initBoard()
      chess.move('e4')
      service.setComment(chess, '"e4" opening')
      expect(chess.get_comment()).toBe('"e4" opening')
    })
  })
})
