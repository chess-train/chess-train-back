export type GameRecord = {
  id: string
  whitePlayerId?: string
  blackPlayerId?: string
  position: string
  pgn?: string
  date: Date
  ended: boolean
}
