import { GetCurrentUser } from '@/auth/get-current-user.decorator'
import { JwtAuthGuard } from '@/auth/jwt-auth.guard'
import { ChessLogger } from '@/logger/chess.logger'
import { UserRecord } from '@/users/users.interface'
import {
  ConflictException,
  ForbiddenException,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  UseGuards,
  Body,
  Controller,
  Get,
  Post,
} from '@nestjs/common'
import { CreateGameDto } from './dto/create-game.dto'
import { CreateMoveDto } from './dto/create-move.dto'
import { GameRecord } from './game.interface'
import { GameService } from './game.service'

@Controller('games')
@UseGuards(JwtAuthGuard)
export class GameController {
  constructor(private service: GameService, private logger: ChessLogger) {
    this.logger.setContext(GameService.name)
  }

  @Post('/')
  async create(
    @GetCurrentUser() user: UserRecord,
    @Body() createGameDto: CreateGameDto,
  ): Promise<{ game: GameRecord }> {
    const game = await this.service.create(createGameDto, user)
    return { game }
  }

  @Get('/:id')
  async get(
    @GetCurrentUser() user: UserRecord,
    @Param('id', new ParseUUIDPipe({ version: '4' })) id: string,
  ): Promise<{ game: GameRecord }> {
    const game = await this.service.get(id)

    if (!game) {
      this.logger.error(`No game found with id ${id}`)
      throw new NotFoundException('No game found with this id')
    }

    if (![game.blackPlayerId, game.whitePlayerId].includes(user.id)) {
      this.logger.error(`User ${user.id} not allowed to access game ${id}`)
      throw new ForbiddenException('Not authorized')
    }

    return {
      game,
    }
  }

  @Get()
  async getAll(@GetCurrentUser() user: UserRecord): Promise<{ games: GameRecord[] }> {
    const games = await this.service.getByUser(user.id)

    return { games }
  }

  @Post('/:id/move')
  async createMove(
    @GetCurrentUser() user: UserRecord,
    @Param('id', new ParseUUIDPipe({ version: '4' })) id: string,
    @Body() createMoveDto: CreateMoveDto,
  ): Promise<{ game: GameRecord }> {
    let { game } = await this.get(user, id)

    if (game.ended) {
      this.logger.error(`User ${user.id} tried to modify a finished game (${id})`)
      throw new ConflictException('Unable to modify a finished game')
    }

    const updatedGame = await this.service.move(game, createMoveDto, user)
    if (updatedGame) {
      return { game: updatedGame }
    }
    return { game }
  }

  @Post('/:id/resign')
  async resign(
    @GetCurrentUser() user: UserRecord,
    @Param('id', new ParseUUIDPipe({ version: '4' })) id: string,
  ): Promise<{ game: GameRecord }> {
    let { game } = await this.get(user, id)

    if (game.ended) {
      this.logger.error(`User ${user.id} tried to modify a finished game (${id})`)
      throw new ConflictException('Unable to modify a finished game')
    }

    const updatedGame = await this.service.resign(game, user)
    if (updatedGame) {
      return { game: updatedGame }
    }
    return { game }
  }

  @Get(':id/move/valid')
  async getValidMoves(
    @GetCurrentUser() user: UserRecord,
    @Param('id', new ParseUUIDPipe({ version: '4' })) id: string,
  ): Promise<{ color: 'b' | 'w'; moves: string[] }> {
    const { game } = await this.get(user, id)

    return this.service.getValidMoves(game)
  }
}
