import { ChessjsModule } from '@/chessjs/chessjs.module'
import { LoggerModule } from '@/logger/logger.module'
import { Module } from '@nestjs/common'
import { GameController } from './game.controller'
import { GameRepository } from './game.repository'
import { GameService } from './game.service'

@Module({
  controllers: [GameController],
  providers: [GameService, GameRepository],
  imports: [ChessjsModule, LoggerModule],
})
export class GameModule {}
