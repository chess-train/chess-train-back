import { GameRecord } from './game.interface'

export class Game implements GameRecord {
  id: string
  whitePlayerId?: string
  blackPlayerId?: string
  position: string
  pgn?: string
  date: Date
  ended: boolean

  constructor(gameRecord: GameRecord) {
    this.id = gameRecord.id
    this.position = gameRecord.position
    this.date = gameRecord.date
    this.ended = gameRecord.ended

    if (gameRecord.pgn) {
      this.pgn = gameRecord.pgn
    }
    if (gameRecord.whitePlayerId) {
      this.whitePlayerId = gameRecord.whitePlayerId
    }
    if (gameRecord.blackPlayerId) {
      this.blackPlayerId = gameRecord.blackPlayerId
    }
  }
}
