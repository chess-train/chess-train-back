import { ChessjsService } from '@/chessjs/chessjs.service'
import { ForbiddenException, Injectable } from '@nestjs/common'
import { CreateGameDto } from './dto/create-game.dto'
import { GameRecord } from './game.interface'
import { GameRepository } from './game.repository'
import { v4 } from 'uuid'
import { UserRecord } from '@/users/users.interface'
import { ChessInstance } from 'chess.js'
import { Game } from './game.class'
import { CreateMoveDto } from './dto/create-move.dto'

@Injectable()
export class GameService {
  constructor(
    private chessJsService: ChessjsService,
    private readonly repository: GameRepository,
  ) {}

  async create(createGameDto: CreateGameDto, user: UserRecord): Promise<Game> {
    const chess = this.chessJsService.initBoard(createGameDto.startingFEN)
    const { date, whitePlayer: whitePlayerId, blackPlayer: blackPlayerId } = createGameDto
    const newGame: GameRecord = {
      id: v4(),
      date: date || new Date(),
      position: chess.fen(),
      whitePlayerId,
      blackPlayerId,
      ended: false,
    }

    try {
      if (![whitePlayerId, blackPlayerId].includes(user.id)) {
        throw new ForbiddenException(
          'User not authorized to create a game with the selected player',
        )
      }
      const dbObject = await this.repository.insert(newGame)
      return new Game(dbObject)
    } catch (err) {
      throw err
    }
  }

  async get(id: string): Promise<Game | undefined> {
    try {
      const dbObject = await this.repository.findById(id)
      return dbObject ? new Game(dbObject) : undefined
    } catch (err) {
      throw err
    }
  }

  async getByUser(userId: string): Promise<Game[]> {
    const games = await this.repository.getByUserId(userId)
    return games.map((game) => new Game(game))
  }

  async move(
    game: GameRecord,
    createMoveDto: CreateMoveDto,
    user: UserRecord,
  ): Promise<GameRecord | undefined> {
    const { move, comment } = createMoveDto

    let chess = this.chessJsService[game.pgn ? 'initBoardFromPgn' : 'initBoard'](
      game.pgn ? game.pgn : game.position,
    )

    if (game.whitePlayerId && game.blackPlayerId) {
      const userColor = game.whitePlayerId === user.id ? 'w' : 'b'

      if (!this.chessJsService.canColorPlay(userColor, chess)) {
        throw new ForbiddenException('Player not allowed to move')
      }
    }

    try {
      chess = this.chessJsService.move(move, chess)

      if (comment) {
        chess = this.chessJsService.setComment(chess, comment)
      }

      const { chess: chessWithTags, ended } = this.postMove(chess)
      return this.repository.updateById(game.id, {
        position: chessWithTags.fen(),
        pgn: chessWithTags.pgn(),
        ended,
      })
    } catch (err) {
      throw err
    }
  }

  async resign(game: GameRecord, user: UserRecord): Promise<GameRecord | undefined> {
    let chess = this.chessJsService[game.pgn ? 'initBoardFromPgn' : 'initBoard'](
      game.pgn ? game.pgn : game.position,
    )

    if (game.whitePlayerId && game.blackPlayerId) {
      const userColor = game.whitePlayerId === user.id ? 'w' : 'b'

      if (!this.chessJsService.canColorPlay(userColor, chess)) {
        throw new ForbiddenException('Player not allowed to play')
      }
    }

    try {
      const currentColor = this.chessJsService.getTurn(chess)
      chess.header(
        'Result',
        `${currentColor === 'w' ? '0' : '1'}-${currentColor === 'b' ? '0' : '1'}`,
      )
      chess.header('Termination', 'abandoned')

      return this.repository.updateById(game.id, {
        position: chess.fen(),
        pgn: chess.pgn(),
        ended: true,
      })
    } catch (err) {
      throw err
    }
  }

  getValidMoves(game: GameRecord): { color: 'b' | 'w'; moves: string[] } {
    let chess = this.chessJsService[game.pgn ? 'initBoardFromPgn' : 'initBoard'](
      game.pgn ? game.pgn : game.position,
    )
    return {
      color: this.chessJsService.getTurn(chess),
      moves: this.chessJsService.getValidMoves(chess),
    }
  }

  postMove(chess: ChessInstance): { chess: ChessInstance; ended: boolean } {
    const { state, winner } = this.chessJsService.getState(chess)
    let resultTag = '*'

    switch (state) {
      case 'DRAW':
        resultTag = '1/2-1/2'
        break
      case 'GAME_OVER':
        resultTag = `${winner === 'w' ? '1' : '0'}-${winner === 'b' ? '1' : '0'}`
        break
    }

    chess.header('Result', resultTag)

    return { chess, ended: resultTag != '*' }
  }

  setComment(chess: ChessInstance, comment: string) {
    chess.set_comment(comment)
    return chess
  }
}
