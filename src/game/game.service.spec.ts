import { Test, TestingModule } from '@nestjs/testing'
import { GameRepository } from './game.repository'
import { GameService } from './game.service'
import { mocked } from 'ts-jest/utils'
import { ChessjsModule } from '@/chessjs/chessjs.module'
import { UserRecord } from '@/users/users.interface'
import { Game } from './game.class'
import { BadRequestException, ForbiddenException } from '@nestjs/common'
import { GameRecord } from './game.interface'

const blackPlayer: UserRecord = {
  id: '0aa5f83d-818f-4462-87c8-e5f148eee87e',
  givenName: 'given name',
  familyName: 'family name',
  authId: 'azertyuiop',
  authProvider: 'google',
  email: 'e@mail.com',
}

const whitePlayer: UserRecord = {
  id: '150D709C-79E3-4508-AC3D-610BADAA5F04',
  givenName: 'given name',
  familyName: 'family name',
  authId: 'azertyuiop',
  authProvider: 'google',
  email: 'e@mail.com',
}

const storedGame: GameRecord = {
  id: '0ff5f83d-818f-4462-87c8-e5f148eee87d',
  position: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1',
  date: new Date('2021-03-14T16:12:30.822Z'),
  blackPlayerId: blackPlayer.id,
  whitePlayerId: whitePlayer.id,
  ended: false,
}
const game: Game = new Game(storedGame)

const whiteStartingValidMoves = [
  'a3',
  'a4',
  'b3',
  'b4',
  'c3',
  'c4',
  'd3',
  'd4',
  'e3',
  'e4',
  'f3',
  'f4',
  'g3',
  'g4',
  'h3',
  'h4',
  'Na3',
  'Nc3',
  'Nf3',
  'Nh3',
]

describe('GameService', () => {
  let service: GameService
  let repository: GameRepository

  beforeEach(async () => {
    const repositoryMock = {
      insert: jest.fn(),
      findById: jest.fn(),
      updateById: jest.fn(),
      getByUserId: jest.fn(),
    }

    const module: TestingModule = await Test.createTestingModule({
      providers: [GameService, { provide: GameRepository, useValue: repositoryMock }],
      imports: [ChessjsModule],
    }).compile()

    service = module.get<GameService>(GameService)
    repository = module.get<GameRepository>(GameRepository)
  })

  describe('get()', () => {
    it('Should get a game by its ID', async () => {
      mocked(repository.findById).mockResolvedValue(storedGame)
      const testGame = await service.get('0ff5f83d-818f-4462-87c8-e5f148eee87d')

      expect(testGame).toEqual(storedGame)
      expect(repository.findById).toBeCalledWith('0ff5f83d-818f-4462-87c8-e5f148eee87d')
    })
  })

  describe('getByUser()', () => {
    it('Should return all games where the current user is the white or black player', async () => {
      mocked(repository.getByUserId).mockResolvedValue([storedGame])

      const blackPlayerGames = await service.getByUser(blackPlayer.id)
      const whitePlayerGames = await service.getByUser(whitePlayer.id)

      expect(blackPlayerGames).toEqual(whitePlayerGames)
      expect(blackPlayerGames).toEqual([storedGame])
    })
  })

  describe('create()', () => {
    it('Should create a game and return it', async () => {
      mocked(repository.insert).mockResolvedValue({
        ...storedGame,
      })

      const gameResult = await service.create(
        {
          startingFEN: storedGame.position,
          date: storedGame.date,
          blackPlayer: storedGame.blackPlayerId,
        },
        blackPlayer,
      )

      expect(gameResult).toStrictEqual(game)
      expect(repository.insert).toBeCalledWith(
        expect.objectContaining({
          blackPlayerId: storedGame.blackPlayerId,
          date: storedGame.date,
          position: storedGame.position,
        }),
      )
    })

    it('Should get an error while trying to create a game with unauthorized users', async () => {
      try {
        await service.create(
          {
            startingFEN: storedGame.position,
            date: storedGame.date,
            blackPlayer: storedGame.blackPlayerId,
          },
          { ...blackPlayer, id: '23256020-174D-47E4-80D9-7CE8FFD1237F' },
        )
      } catch (err) {
        expect(err.message).toBe('User not authorized to create a game with the selected player')
        expect(err instanceof ForbiddenException).toBeTruthy()
      }
    })
  })

  describe('getValidMoves()', () => {
    it('Should return the valid moves for white', async () => {
      expect(service.getValidMoves(storedGame)).toStrictEqual({
        color: 'w',
        moves: whiteStartingValidMoves,
      })
    })
  })

  describe('move()', () => {
    beforeEach(async () => {
      mocked(repository.updateById).mockImplementation(
        async (id: string, params: Partial<GameRecord>) => ({
          ...storedGame,
          ...params,
        }),
      )
    })
    it('Should get an error when black player is trying to play instead of white player', async () => {
      try {
        await service.move(storedGame, { move: 'e6' }, blackPlayer)
      } catch (err) {
        expect(err instanceof ForbiddenException).toBeTruthy()
        expect(err.message).toBe('Player not allowed to move')
      }
    })

    it('Should get an error when trying to play an invalid move', async () => {
      try {
        await service.move(storedGame, { move: 'e6' }, whitePlayer)
      } catch (err) {
        expect(err instanceof BadRequestException).toBeTruthy()
        expect(err.message).toBe('Forbidden or invalid move')
      }
    })

    it('Should move and return the updated GameRecord', async () => {
      let testGame = await service.move(storedGame, { move: 'e4' }, whitePlayer)
      expect(repository.updateById).toBeCalledWith(testGame?.id, {
        position: 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1',
        pgn: '[Result "*"]\n\n1. e4 *',
        ended: false,
      })

      testGame = await service.move(testGame!, { move: 'Nc6' }, blackPlayer)
      expect(repository.updateById).toBeCalledWith(testGame?.id, {
        position: 'r1bqkbnr/pppppppp/2n5/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 1 2',
        pgn: '[Result "*"]\n\n1. e4 Nc6 *',
        ended: false,
      })
    })

    it('Should move and add a comment then return the updated GameRecord', async () => {
      let game = await service.move(
        storedGame,
        { move: 'e4', comment: '"e4" opening' },
        whitePlayer,
      )
      expect(repository.updateById).toBeCalledWith(game?.id, {
        position: 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1',
        pgn: '[Result "*"]\n\n1. e4 {"e4" opening} *',
        ended: false,
      })
    })

    it(`Should move for the other color if there isn't any player linked`, async () => {
      let game = await service.move(
        { ...storedGame, whitePlayerId: undefined },
        { move: 'e4' },
        blackPlayer,
      )
      expect(repository.updateById).toBeCalledWith(game?.id, {
        position: 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1',
        pgn: '[Result "*"]\n\n1. e4 *',
        ended: false,
      })

      game = await service.move(
        { ...game!, blackPlayerId: undefined, whitePlayerId: storedGame.whitePlayerId },
        { move: 'Nc6' },
        blackPlayer,
      )
      expect(repository.updateById).toBeCalledWith(game?.id, {
        position: 'r1bqkbnr/pppppppp/2n5/8/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 1 2',
        pgn: '[Result "*"]\n\n1. e4 Nc6 *',
        ended: false,
      })
    })

    it(`Should move and finish the game`, async () => {
      let game = await service.move(
        {
          ...storedGame,
          position: '6k1/p4p2/1p2r2p/P7/1P1p2Pp/3P1q1n/3n3K/4b3 b - - 1 37',
        },
        { move: 'Re2#', comment: 'The rook ends the game' },
        blackPlayer,
      )

      expect(repository.updateById).toBeCalledWith(
        game?.id,
        expect.objectContaining({
          position: '6k1/p4p2/1p5p/P7/1P1p2Pp/3P1q1n/3nr2K/4b3 w - - 2 38',
          ended: true,
          pgn: expect.stringContaining('[Result "0-1"]'),
        }),
      )
    })
  })

  describe('resign()', () => {
    beforeEach(async () => {
      mocked(repository.updateById).mockImplementation(
        async (id: string, params: Partial<GameRecord>) => ({
          ...storedGame,
          ...params,
        }),
      )
    })
    it('Should get an error when black player is trying to play instead of white player', async () => {
      try {
        await service.resign(storedGame, blackPlayer)
      } catch (err) {
        expect(err instanceof ForbiddenException).toBeTruthy()
        expect(err.message).toBe('Player not allowed to play')
      }
    })

    it(`Should play for the other color if there isn't any player linked`, async () => {
      let game = await service.resign({ ...storedGame, whitePlayerId: undefined }, blackPlayer)
      expect(repository.updateById).toBeCalledWith(game?.id, {
        position: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1',
        pgn: `[Result "0-1"]\n[Termination "abandoned"]\n 0-1`,
        ended: true,
      })
    })
  })
})
