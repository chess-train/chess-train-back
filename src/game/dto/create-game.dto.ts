import { IsOptional, IsString, IsUUID } from 'class-validator'

export class CreateGameDto {
  @IsString()
  @IsOptional()
  startingFEN?: string

  @IsUUID()
  @IsOptional()
  whitePlayer?: string

  @IsUUID()
  @IsOptional()
  blackPlayer?: string

  @IsOptional()
  date?: Date
}
