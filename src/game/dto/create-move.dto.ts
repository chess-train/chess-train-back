import { IsString } from 'class-validator'

export class CreateMoveDto {
  @IsString()
  move!: string

  @IsString()
  comment?: string
}
