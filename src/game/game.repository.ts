import { KnexRepository } from '@/knex/knex.repository'
import { GameRecord } from './game.interface'

export class GameRepository extends KnexRepository<GameRecord> {
  public tableName = 'game'

  async getByUserId(userId: string): Promise<GameRecord[]> {
    const { rows } = await this.knex.raw(
      `SELECT * FROM game WHERE "whitePlayerId" = '${userId}' OR "blackPlayerId" = '${userId}' ORDER BY date DESC`,
    )

    return rows
  }
}
