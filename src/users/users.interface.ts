export type UserRecord = {
  id: string
  givenName: string
  familyName: string
  email: string
  authProvider: string
  authId: string
}
