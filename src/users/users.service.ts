import { Injectable } from '@nestjs/common'
import { UserRecord } from './users.interface'
import { UsersRepository } from './users.repository'

@Injectable()
export class UsersService {
  constructor(private readonly repository: UsersRepository) {}

  async get(id: string): Promise<UserRecord | undefined> {
    return this.repository.findById(id)
  }

  async find(params: any): Promise<UserRecord[]> {
    return this.repository.find(params)
  }

  async create(user: UserRecord): Promise<UserRecord> {
    return this.repository.insert(user)
  }
}
