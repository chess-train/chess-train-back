import { KnexRepository } from '@/knex/knex.repository'
import { UserRecord } from './users.interface'

export class UsersRepository extends KnexRepository<UserRecord> {
  public tableName = 'user'
}
