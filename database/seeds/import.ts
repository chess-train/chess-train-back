import { Knex } from 'knex'

export async function seed(knex: Knex): Promise<any> {
  const datasets: Record<string, any> = {}

  for (const tableName of Object.keys(datasets)) {
    await knex(tableName).insert(datasets[`${tableName}`])
  }
}
