import { triggerUpdate } from '../../src/knex/knex.helper'
import { Knex } from 'knex'

const tableName = 'game'

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable(tableName, function (table: Knex.CreateTableBuilder) {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
    table
      .uuid('whitePlayerId')
      .references('user.id')
      .onDelete('CASCADE')
      .onUpdate('CASCADE')
      .index()
    table
      .uuid('blackPlayerId')
      .references('user.id')
      .onDelete('CASCADE')
      .onUpdate('CASCADE')
      .index()
    table.string('position').notNullable()
    table.string('pgn')
    table.timestamp('date').defaultTo(knex.fn.now())
    table.timestamp('updatedAt').defaultTo(null)
  })
  await knex.raw(triggerUpdate(tableName))
}
export async function down(knex: Knex): Promise<any> {
  return knex.schema.dropTable(tableName)
}
