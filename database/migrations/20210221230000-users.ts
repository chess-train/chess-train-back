import { triggerUpdate } from '../../src/knex/knex.helper'
import { Knex } from 'knex'

const tableName = 'user'

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable(tableName, function (table: Knex.CreateTableBuilder) {
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
    table.string('givenName').notNullable()
    table.string('familyName').notNullable()
    table.string('email').notNullable()
    table.string('authProvider').notNullable()
    table.string('authId').notNullable()
    table.timestamp('createdAt').defaultTo(knex.fn.now())
    table.timestamp('updatedAt').defaultTo(null)
  })
  await knex.raw(triggerUpdate(tableName))
}
export async function down(knex: Knex): Promise<any> {
  return knex.schema.dropTable(tableName)
}
