import { updateTriggerCode } from '../../src/knex/knex.helper'
import { Knex } from 'knex'

export async function up(knex: Knex): Promise<any> {
  return Promise.all([
    knex.raw('create extension if not exists "uuid-ossp"'),
    knex.raw(updateTriggerCode),
  ])
}

export async function down(): Promise<any> {
  return Promise.resolve(true)
}
