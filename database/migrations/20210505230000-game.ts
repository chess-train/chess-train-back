import { Knex } from 'knex'

export async function up(knex: Knex): Promise<any> {
  await knex.raw(`ALTER TABLE game ADD COLUMN IF NOT EXISTS ended BOOLEAN DEFAULT FALSE`)
}
export async function down(knex: Knex): Promise<any> {
  return knex.raw(`ALTER TABLE game DROP COLUMN IF EXISTS ended`)
}
